//
//  BBCharacter.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import Foundation
import SwiftyJSON

struct BBCharacter {
    let name: String
    let appearances: [String]
    var appearanceString: String {
        guard appearances.count > 0 else { return "No appearences" }
        var result = ""
        for appearance in appearances {
            result.append("\(result.isEmpty ? "" : ", ")Season \(appearance)")
        }
        return result
    }
    let occupations: [String]
    var occupationString: String {
        guard occupations.count > 0 else { return "No occupation" }
        var result = ""
        for occupation in occupations {
             result.append("\(result.isEmpty ? "" : ", ")\(occupation)")
        }
        return result
    }
    let status: String
    let nickname: String
    let image: String
    var imageURL: URL? {
        return URL(string: image)
    }

    init(json: JSON) {
        name = json["name"].stringValue
        image = json["img"].stringValue
        let appearancesJSON = json["appearance"]
        var appearancesArray = [String]()
        for appearance in appearancesJSON.arrayValue {
            appearancesArray.append(appearance.stringValue)
        }
        appearances = appearancesArray
        let occupationsJSON = json["occupation"]
        var occupationsArray = [String]()
        for occupation in occupationsJSON.arrayValue {
            occupationsArray.append(occupation.stringValue)
        }
        occupations = occupationsArray
        status = json["status"].stringValue
        nickname = json["nickname"].stringValue
    }
}
