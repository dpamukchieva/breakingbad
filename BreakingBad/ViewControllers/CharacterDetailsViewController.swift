//
//  CharacterDetailsViewController.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import UIKit

class CharacterDetailsViewController: UIViewController {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var nicknameLabel: UILabel!
    @IBOutlet private weak var occupationLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var seasonsLabel: UILabel!
    @IBOutlet private weak var nameTitleLabel: UILabel!
    @IBOutlet private weak var nicknameTitleLabel: UILabel!
    @IBOutlet private weak var occupationTitleLabel: UILabel!
    @IBOutlet private weak var statusTitleLabel: UILabel!
    @IBOutlet private weak var seasonsTitleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!

    var character: BBCharacter?

    override func viewDidLoad() {
        super.viewDidLoad()
        populateForCharacter()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imageView.layer.cornerRadius = imageView.bounds.size.height/2.0
    }

    // MARK: - UI

    private func populateForCharacter() {
        guard let character = character else { return }
        nameLabel.text = character.name
        nameTitleLabel.isHidden = character.name.isEmpty
        nicknameLabel.text = character.nickname
        nicknameTitleLabel.isHidden = character.nickname.isEmpty
        occupationLabel.text = character.occupationString
        occupationTitleLabel.isHidden = character.occupationString.isEmpty
        statusLabel.text = character.status
        statusTitleLabel.isHidden = character.status.isEmpty
        seasonsLabel.text = character.appearanceString
        seasonsTitleLabel.isHidden = character.appearanceString.isEmpty

        if let url = character.imageURL {
            imageView.af_setImage(withURL: url)
        }
    }

    // MARK: - Actions

    @IBAction func dismissButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
