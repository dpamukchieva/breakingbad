//
//  CharactersListViewController.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import UIKit
import AlamofireImage

class CharactersListViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var seasonTextField: UITextField!
    @IBOutlet private weak var noResultsLabel: UILabel!
    @IBOutlet private weak var pickerView: UIPickerView!
    @IBOutlet private weak var pickerSheet: UIView!

    private var allCharacters = [BBCharacter]()
    private var filteredCharacters = [BBCharacter]()
    private var seasons = ["", "1", "2", "3", "4", "5"]
    private var selectedSeason = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        fetchData()
        seasonTextField.inputView = pickerSheet
    }

    // MARK: - Data

    private func fetchData() {
        HTTPClient().getCharacters { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                sSelf.allCharacters.append(contentsOf: result)
                sSelf.filter()
                sSelf.tableView.reloadData()
            }
        }
    }

    private func filter() {
        guard let searchTerm = searchTextField.text?.lowercased()else { return }
        filteredCharacters = allCharacters.filter { (searchTerm.isEmpty || $0.name.lowercased().contains(searchTerm)) && (selectedSeason.isEmpty || $0.appearances.contains(selectedSeason)) }
        filteredCharacters.sort { (character1, character2) -> Bool in
            let name1 = character1.name.lowercased()
            let name2 = character2.name.lowercased()
            if (name1.hasPrefix(searchTerm) && name2.hasPrefix(searchTerm)) || (!name1.hasPrefix(searchTerm) && !name2.hasPrefix(searchTerm)) {
                return name1.compare(name2) == .orderedAscending
            } else {
               return !name2.hasPrefix(searchTerm)
            }
        }
    }

    // MARK: - Actions

    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        filter()
        tableView.reloadData()
    }

    @IBAction func doneButtonPressed(_ sender: UIButton) {
        selectedSeason = seasons[pickerView.selectedRow(inComponent: 0)]
        seasonTextField.text = selectedSeason.isEmpty ? "" : "Season \(selectedSeason)"
        view.endEditing(true)
        filter()
        tableView.reloadData()
    }
}

// MARK: - UITableView Methods

extension CharactersListViewController: UITableViewDataSource, UITableViewDelegate {

    private func setupTableView() {
        tableView.register(UINib(nibName: Constants.Identifier.Cell.characterCell, bundle: nil), forCellReuseIdentifier: Constants.Identifier.Cell.characterCell)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noResultsLabel.isHidden = filteredCharacters.count > 0 || !searchTextField.hasText
        tableView.isHidden = filteredCharacters.count == 0
        return filteredCharacters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifier.Cell.characterCell) as! CharacterTableViewCell
        cell.imgView.image = nil

        let character = filteredCharacters[indexPath.row]
        cell.nameLabel.text = character.name
        if let url = character.imageURL {
            cell.imgView.af_setImage(withURL: url)
        } else {
            cell.imgView.image = nil
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.Number.cellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailsViewController = storyboard?.instantiateViewController(withIdentifier: Constants.Identifier.ViewController.detailsViewController) as? CharacterDetailsViewController {
            detailsViewController.character = filteredCharacters[indexPath.row]
            present(detailsViewController, animated: true, completion: nil)
        }
    }
}

// MARK: - UITableView Methods

extension CharactersListViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == seasonTextField {
            pickerSheet.isHidden = false
        }

        return true
    }
}

// MARK: - UITableView Methods

extension CharactersListViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return seasons.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return seasons[row].isEmpty ? "" : "Season \(seasons[row])"
    }
}
