//
//  HTTPClient.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate enum HttpMethod {
    case GET

    func stringValue() -> String {
        switch self {
        case .GET: return "GET"
        }
    }
}

fileprivate enum APIEndpoint {
    case characters

    func stringValue() -> String {
        switch self {
        case .characters: return "characters"
        }
    }
}

class HTTPClient {

    func getCharacters(completion: @escaping (_ result: [BBCharacter]) -> ()) {
        serverRequest(type: .GET, method: .characters) { (data, error) in
            var characters = [BBCharacter]()
            if let data = data, error == nil {
                do {
                    let charactersJSON = try JSON(data: data)
                    for characterJSON in charactersJSON.arrayValue {
                        let character = BBCharacter(json: characterJSON)
                        characters.append(character)
                    }
                } catch {
                    print(error.localizedDescription)
                }

                completion(characters)
            }
        }
    }


    // MARK: - Private

    private func serverRequest(type: HttpMethod, method: APIEndpoint, params: [String: String]? = nil, body: String? = nil, withCompletion completion: @escaping (_ data: Data?, _ error: Error?) -> ()) {
        let urlString = Constants.URL.base + method.stringValue()

        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)

        request.httpMethod = type.stringValue()

        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in

            completion(data, error)
        })
        task.resume()
    }

}
