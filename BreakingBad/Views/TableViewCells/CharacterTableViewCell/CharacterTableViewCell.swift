//
//  CharacterTableViewCell.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imgView.layer.cornerRadius = imgView.bounds.size.height/2.0
    }
}
