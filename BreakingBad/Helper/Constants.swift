//
//  Constants.swift
//  BreakingBad
//
//  Created by Dea Pamukchieva on 17.02.20.
//  Copyright © 2020 Dea. All rights reserved.
//

import UIKit

struct Constants {

    struct Identifier {
        struct ViewController {
            static let listViewController = "CharactersListViewController"
            static let detailsViewController = "CharacterDetailsViewController"
        }

        struct Cell {
            static let characterCell = "CharacterTableViewCell"
        }
    }

    struct URL {
        static let base = "https://breakingbadapi.com/api/"
    }

    struct Number {
        static let cellHeight: CGFloat = 80.0
    }
}
